﻿//using System;
//using PubNubMessaging.Core;
//
//namespace emotiv_app
//{
//	public class Client
//	{
//		const string PUBLISH_KEY = "pub-c-3a5fa759-80a2-4619-9236-73143b17783e";
//		const string SUBSCRIBE_KEY = "sub-c-327e10f2-ebda-11e3-b601-02ee2ddab7fe";
//
//		const string CHANNEL = "intropacific";
//
//		private Pubnub _pubnub;
//
//		private bool subscribed;
//
//		public static readonly Client INSTANCE = new Client ();
//
//		private Client ()
//		{
//		}
//
//		public void Subscribe (Action<string> messageReceived)
//		{
//			if (subscribed) {
//				Console.WriteLine ("Already subscribed");
//				return;
//			}
//			if (_pubnub != null) {
//				Console.WriteLine ("Already subscribing");
//				return;
//			}
//			_pubnub = new Pubnub (PUBLISH_KEY, SUBSCRIBE_KEY);
//			_pubnub.Subscribe<string> (
//				CHANNEL,
//				delegate (string message) {
//					try {
//						if (messageReceived != null) {
//							JsonValue json = JsonObject.Parse (JsonObject.Parse (message)[0]);
//							string data = json.ToString();
//							messageReceived (data);
//						}
//					} catch (Exception e) {
//						Console.WriteLine ("Unexpected exception: " + e.Message);
//						Console.WriteLine (e.StackTrace);
//					}
//				},
//				delegate (string message) {
//					Console.WriteLine ("pubnub connect: " + message);
//					subscribed = true;
//				},
//				delegate (PubnubClientError err) {
//					Console.WriteLine ("pubnub error: " + err.Description);
//				}
//			);
//		}
//
//		public void UnSubscribe ()
//		{
//			_pubnub.EndPendingRequests ();
//			_pubnub.Unsubscribe<string>(
//				CHANNEL,
//				delegate {
//					try {
//						_pubnub = null;
//						subscribed = false;
//					} catch (Exception e) {
//						Console.WriteLine ("Unexpected exception: " + e.Message);
//						Console.WriteLine (e.StackTrace);
//					}
//				},
//				delegate {
//					try {
//						_pubnub = null;
//						subscribed = false;
//					} catch (Exception e) {
//						Console.WriteLine ("Unexpected exception: " + e.Message);
//						Console.WriteLine (e.StackTrace);
//					}
//				},
//				delegate {
//					try {
//						_pubnub = null;
//						subscribed = false;
//					} catch (Exception e) {
//						Console.WriteLine ("Unexpected exception: " + e.Message);
//						Console.WriteLine (e.StackTrace);
//					}
//				},
//				delegate {
//					try {
//						_pubnub = null;
//						subscribed = false;
//					} catch (Exception e) {
//						Console.WriteLine ("Unexpected exception: " + e.Message);
//						Console.WriteLine (e.StackTrace);
//					}
//				}
//			);
//		}
//
//		public void Publish (LocationInfo locationInfo)
//		{
//			Publish (new JsonObject {
//				{"name", locationInfo.Name},
//				{"rssi", locationInfo.RSSI},
//				{"id", locationInfo.ID}
//			});
//		}
//
//		private void Publish (JsonValue json)
//		{
//			if (_pubnub == null) {
//				Console.WriteLine ("pubnub not subscribed, not publishing message");
//				return;
//			}
//			if (!subscribed) {
//				Console.WriteLine ("pubnub subscribe in progress, not publishing message");
//				return;
//			}
//			_pubnub = new Pubnub (PUBLISH_KEY, SUBSCRIBE_KEY);
//			string message = json.ToString ();
//			Console.WriteLine ("publishing message " + message);
//			_pubnub.Publish (
//				CHANNEL,
//				message,
//				delegate {
//					Console.WriteLine ("publishing message successful");
//				}, 
//				delegate {
//					Console.WriteLine ("publishing message failed");
//				}
//			);
//		}
//	}
//}
//
