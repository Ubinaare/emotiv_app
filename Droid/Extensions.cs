﻿using System;

namespace emotiv_app.Droid
{
	public static class Extensions
	{
		public static byte[] ToBytes (this string str)
		{
			byte[] bytes = new byte[str.Length * sizeof(char)];
			System.Buffer.BlockCopy(str.ToCharArray(), 0, bytes, 0, bytes.Length);
			return bytes;
		}
	}
}

