﻿using System;
using Android.App;
using Android.Graphics;

namespace emotiv_app.Droid
{
	public class MainView : UIView
	{
		UILabel connectedLabel, servicesLabel;

		public UIView WakeupButton { get; set; }

		public UIView ColorButton { get; set; }

		public MainView (Activity context) : base(context)
		{
			BackgroundColor = Color.Rgb(240, 240, 240);

			connectedLabel = GetLabelWithText (context, "Connecting...");

			servicesLabel = GetLabelWithText (context, "Fetching services and characteristics...");

			WakeupButton = new UIView (context);

			ColorButton = new UIView (context);

			AddViews (connectedLabel, servicesLabel, WakeupButton, ColorButton);
		}

		UILabel GetLabelWithText(Activity context, string text)
		{
			return new UILabel (context) {
				Text = text,
				TextColor = Color.Black,
				TextAlignment = Android.Views.TextAlignment.Center
			};

		}

		public override void LayoutSubviews ()
		{
			int padding = Device.ScreenWidth / 10;

			int x = padding;
			int y = padding;
			int w = Device.ScreenWidth - 2 * padding;
			int h = 100;

			connectedLabel.Frame = new Frame (x, y, w, h);

			y += h + padding;

			servicesLabel.Frame = new Frame (x, y, w, h);

			y += h + padding;
			w = (Device.ScreenWidth - 3 * padding) / 2;
			h = (int)(w / 2.5f);

			WakeupButton.Frame = new Frame (x, y, w, h);
			WakeupButton.SetSlightlyRoundWithBackgroundColor (Color.Red);

			y += h + padding;

			ColorButton.Frame = new Frame (x, y, w, h);
			ColorButton.SetSlightlyRoundWithBackgroundColor (Color.Green);
		}

		public void UpdateConnectedText ()
		{
			connectedLabel.Text = "Connected";
		}

		public void UpdateServicesText ()
		{
			servicesLabel.Text = "Discovered services and characteristics";
		}
	}
}

