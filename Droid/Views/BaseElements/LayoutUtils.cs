﻿using System;
using Android.Widget;

namespace emotiv_app.Droid
{
	public class LayoutUtils
	{
		public static RelativeLayout.LayoutParams GetRelative (int x, int y, int w, int h)
		{
			RelativeLayout.LayoutParams parameters = new RelativeLayout.LayoutParams(w, h);
			parameters.LeftMargin = x;
			parameters.TopMargin = y;

			return parameters;
		}

		public static ScrollView.LayoutParams GetScrollView (int x, int y, int w, int h)
		{
			ScrollView.LayoutParams parameters = new ScrollView.LayoutParams (w, h);
			parameters.LeftMargin = x;
			parameters.TopMargin = y;

			return parameters;
		}

		public static FrameLayout.LayoutParams GetFrame (int x, int y, int w, int h)
		{
			FrameLayout.LayoutParams parameters = new FrameLayout.LayoutParams (w, h);
			parameters.LeftMargin = x;
			parameters.TopMargin = y;

			return parameters;
		}
	}
}

