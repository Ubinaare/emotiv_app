﻿using System;
using System.Collections.Generic;

namespace emotiv_app.Droid
{
	public enum FontStyle
	{
		Serif,
		SerifLight,
		SerifThin,
		SerifCondensed
	}

	public class Font
	{
		const int maxTitleSize = 42;
		const int maxSmallTitleSize = 35;
		const int maxSmallestTitleSize = 30;
		const int maxSmallSize = 25;

		public static int TitleSize {
			get {
				int size = (int)(Device.ScreenHeight / 35);

				if (size > maxTitleSize && Device.IsTablet) {
					return maxTitleSize;
				}

				return size;
			}
		}

		public static int SmallTitleSize {
			get {
				int size = (int)(Device.ScreenHeight / 41);

				if (size > maxSmallTitleSize && Device.IsTablet) {
					return maxSmallTitleSize;
				}

				return size;
			}
		}

		public static int SmallestTitleSize {
			get {
				int size = (int)(Device.ScreenHeight / 47);

				if (size > maxSmallestTitleSize && Device.IsTablet) {
					return maxSmallestTitleSize;
				}

				return size;
			}
		}

		public static int SmallSize {
			get {
				int size = (int)(Device.ScreenHeight / 55);

				if (size > maxSmallSize && Device.IsTablet) {
					return maxSmallSize;
				}

				return size;
			}
		}

		public static Dictionary<int, string> EnumStringValues = new Dictionary<int, string> {
			{ 0, "sans-serif" },
			{ 1, "sans-serif-light" },
			{ 2, "sans-serif-thin" },
			{ 3, "sans-serif-condensed" },
		};

		public static Font Get (FontStyle style, int size, bool bold = false, bool italic = false)
		{
			return new Font () { Name = EnumStringValues[(int)style], Size = size, Bold = bold, Italic = italic };
		}

		public string Name { get; set; }

		public int Size { get; set; }

		public bool Bold { get; set; }

		public bool Italic { get; set; }
	}
}

