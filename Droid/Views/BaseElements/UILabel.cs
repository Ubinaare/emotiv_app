﻿using System;
using Android.Widget;
using Android.Graphics;
using Android.App;
using Android.Animation;
using Java.Interop;
using Android.OS;

namespace emotiv_app.Droid
{
	public class UILabel : TextView
	{
		public const string LEFT_MARGIN = "LeftMargin";
		public const string TOP_MARGIN = "TopMargin";
		public const string NEW_WIDTH = "NewWidth";

		Frame frame;
		Color color;
		Color backgroundColor;
		Font font;

		public Frame Frame {
			get {
				return frame;
			}
			set {
				frame = value;
				LayoutParameters = LayoutUtils.GetRelative (frame.X, frame.Y, frame.W, frame.H);
			}
		}

		public Color TextColor {
			get {
				return color;
			}
			set {
				color = value;
				SetTextColor (color);
			}
		}

		public Color BackgroundColor {
			get {
				return backgroundColor;
			}
			set {
				backgroundColor = value;
				SetBackgroundColor (backgroundColor);
			}
		}

		public Font Font {
			get {
				return font;
			}
			set {
				font = value;
				if (font.Bold) {
					SetTypeface (Typeface.Create (font.Name, TypefaceStyle.Bold), TypefaceStyle.Bold);
				} else if (font.Italic) {
					SetTypeface (Typeface.Create (font.Name, TypefaceStyle.Italic), TypefaceStyle.Italic);
				} else {
					SetTypeface (Typeface.Create (font.Name, TypefaceStyle.Normal), TypefaceStyle.Normal);
				}

				if (font.Size != 0) {
					SetTextSize (Android.Util.ComplexUnitType.Px, font.Size);
				}
			}
		}

		public ObjectAnimator AlphaOutAnimator {
			get {
				return ObjectAnimator.OfFloat (this, "Alpha", 1, 0);
			}
		}

		public ObjectAnimator AlphaInAnimator {
			get {
				return ObjectAnimator.OfFloat (this, "Alpha", 0, 1);
			}
		}

		public UILabel (Activity context) : base (context)
		{
			
		}

		[Export]
		public float getAlpha ()
		{
			return Alpha;	
		}

		[Export]
		public void setAlpha (float alpha)
		{
			Alpha = alpha;
		}

		[Export]
		public float getLeftMargin ()
		{
			return frame.X;	
		}

		[Export]
		public void setLeftMargin (int newX)
		{
			Frame = new Frame (newX, Frame.Y, Frame.W, Frame.H);
		}

		public virtual void Hide ()
		{
			Alpha = 0;
			Visibility = Android.Views.ViewStates.Gone;
		}

		public virtual void Show ()
		{
			Alpha = 1;
			Visibility = Android.Views.ViewStates.Visible;
		}
			
		public void AnimateX (Frame newFrame, Action completed)
		{
			ObjectAnimator xAnim = ObjectAnimator.OfInt (this, LEFT_MARGIN, Frame.X, newFrame.X);

			xAnim.SetDuration (200);

			xAnim.Start ();

			xAnim.AnimationEnd += delegate {
				new Handler ().Post (completed);
			};
		}

		public void AnimateIn (Action completed)
		{
			ObjectAnimator animator = AlphaInAnimator;	

			animator.SetDuration (200);
			animator.Start ();

			animator.AnimationEnd += delegate {
				new Handler ().Post (completed);
			};
		}

		public void AnimateOut (Action completed)
		{
			ObjectAnimator animator = AlphaOutAnimator;	

			animator.SetDuration (200);
			animator.Start ();

			animator.AnimationEnd += delegate {
				new Handler ().Post (completed);
			};
		}

		public int SizeHeightToFit ()
		{
			return SizeHeightToFitWithMin (0);
		}

		public int SizeHeightToFitWithMin (int min)
		{
			Measure (0, 0);

			int height = MeasuredHeight;

			if (MeasuredHeight < min) {
				height = min;
			}

			if (frame == null) {
				frame = new Frame ();
			}

			Frame = new Frame (frame.X, frame.Y, frame.W, height);
			return height;
		}
			
		public void UpdateHeightAndY (int height, int y)
		{
			Frame = new Frame (frame.X, y, frame.W, height);
		}

		public void UpdateHeight (int height)
		{
			Frame = new Frame (frame.X, frame.Y, frame.W, height);
		}
	}
}

