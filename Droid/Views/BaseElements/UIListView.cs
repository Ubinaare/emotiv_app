﻿using System;
using Android.Widget;
using Android.App;

namespace emotiv_app.Droid
{
	public class UIListView : ListView
	{
		Frame frame;

		public virtual Frame Frame {
			get {
				if (frame == null) {
					return new Frame ();
				}
				return frame;
			} set {
				frame = value;

				RelativeLayout.LayoutParams parameters = new RelativeLayout.LayoutParams(value.W, value.H);
				parameters.LeftMargin = value.X;
				parameters.TopMargin = value.Y;

				LayoutParameters = parameters;

				LayoutSubviews ();
			}
		}

		public UIListView (Activity context) : base (context)
		{
			
		}

		public virtual void LayoutSubviews ()
		{
			
		}

		public override bool OnTouchEvent (Android.Views.MotionEvent e)
		{
			int x = (int)e.GetX ();

			if (IsOnLeftEdgeOfScreen (x)) {
				return false;
			}
			return base.OnTouchEvent (e);
		}

		bool IsOnLeftEdgeOfScreen (int x)
		{
			int drawerArea = Device.ScreenWidth / 30;

			return x <= drawerArea;
		}
	}
}

