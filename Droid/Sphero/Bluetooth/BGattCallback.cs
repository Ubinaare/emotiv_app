﻿using System;
using Android.Bluetooth;

namespace emotiv_app.Droid
{
	public class BGattCallback : BluetoothGattCallback
	{
		private MainActivity activity;

		public EventHandler<EventArgs> OnCharacteristicWritten;

		public BGattCallback(MainActivity activity) 
		{
			this.activity = activity;
		}

		public override void OnConnectionStateChange (BluetoothGatt gatt, GattStatus status, ProfileState newState)
		{
			base.OnConnectionStateChange (gatt, status, newState);

			Console.WriteLine ("OnConnectionStateChange: ", newState.ToString());
			//activity.RunOnUiThread (() => activity.statusLabel.Text = "Connected");
			gatt.DiscoverServices ();
		}

		public override void OnServicesDiscovered (BluetoothGatt gatt, GattStatus status)
		{
//			Console.WriteLine ("Services: " + gatt.Services.Count);

			foreach (BluetoothGattService service in gatt.Services) {
				Console.WriteLine ("Service: " + service.Uuid);
				Console.WriteLine ("Characteristics: ");

				foreach (BluetoothGattCharacteristic characteristic in service.Characteristics) {
					Console.WriteLine (characteristic.Uuid);	
				}
				Console.WriteLine ("-------------------------------------------");
			}

			Console.WriteLine ("OnServicesDiscovered: " + gatt + " - " + status.ToString ());

		}

		public override void OnCharacteristicRead (BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, GattStatus status)
		{
			Console.WriteLine ("OnCharacteristicRead: " + characteristic);
			base.OnCharacteristicRead (gatt, characteristic, status);
		}

		public override void OnDescriptorRead (BluetoothGatt gatt, BluetoothGattDescriptor descriptor, GattStatus status)
		{
			Console.WriteLine ("OnDescriptorRead: " + descriptor);
			base.OnDescriptorRead (gatt, descriptor, status);
		}

		public override void OnCharacteristicWrite (BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, GattStatus status)
		{
			activity.Continue ();

			if (OnCharacteristicWritten != null) {
				OnCharacteristicWritten (new object (), new EventArgs ());
			}

			base.OnCharacteristicWrite (gatt, characteristic, status);
		}
	}
}

