﻿using System;
using Android.Bluetooth;
using Android.App;
using Android.Content;
using System.Collections;
using System.Collections.Generic;

namespace emotiv_app.Droid
{
	public class BluetoothConnection : Java.Lang.Object, BluetoothAdapter.ILeScanCallback
	{
		public EventHandler<EventArgs> OnConnected;

		BluetoothGatt gatt;
		BluetoothAdapter adapter;
		BluetoothDevice device;

		public BluetoothCallback Callback;

		Sphero sphero;

		public Queue<WriteInformation> DataQueue { get; internal set; }

		public Activity Activity {
			get {
				return sphero.Activity;
			}
		}

		public BluetoothConnection (Sphero sphero)
		{
			this.sphero = sphero;

			BluetoothManager manager = (BluetoothManager)Activity.GetSystemService (Activity.BluetoothService);
			adapter = manager.Adapter;

			Callback = new BluetoothCallback ();

			DataQueue = new Queue<WriteInformation> ();

			Callback.OnCharacteristicWritten += ContinueSend;
		}

		public void Queue (List<WriteInformation> dataList)
		{
			foreach (WriteInformation data in dataList) {
				DataQueue.Enqueue (data);
			}

			if (dataList.Count == DataQueue.Count) {
				ContinueSend (new object (), new EventArgs ());
			}
		}
			
		public void Queue (WriteInformation data)
		{
			DataQueue.Enqueue (data);

			if (DataQueue.Count == 1) {
				ContinueSend (new object (), new EventArgs ());
			}
		}

		void ContinueSend (object sender, EventArgs e)
		{
			if (DataQueue.Count == 0) {
				return;
			}

			WriteInformation data = DataQueue.Dequeue ();

			Write (data);
		}

		public void OnLeScan (BluetoothDevice device, int rssi, byte[] scanRecord)
		{
			Console.WriteLine ("Found device: " + device.Name + " with address: " + device.Address);

			if (device.Address == sphero.Address) {
				this.device = device;
				Console.WriteLine ("Connecting to Sphero");
//				Stops scan once it finds one device
				StopScan ();
				Connect ();
			} else {
				Console.WriteLine ("Incorrect device address");
			}
		}

		private void Connect()
		{
			Activity.RunOnUiThread (delegate {
				gatt = device.ConnectGatt (Activity, true, Callback);
				if (OnConnected != null) {
					OnConnected(new object (), new EventArgs ());
				}
			});
		}

		public void Write (WriteInformation information)
		{
			BluetoothGattService service = gatt.GetService (information.Service);

			if (service == null) {
				Console.WriteLine ("Service is empty");
				return;
			}
				
			BluetoothGattCharacteristic characteristic = service.GetCharacteristic (information.Characteristic);

			if (characteristic == null) {
				Console.WriteLine ("Characteristic empty");
				return;
			}

			characteristic.SetValue (information.Data);

			gatt.WriteCharacteristic (characteristic);
		}

		public void Scan ()
		{
			adapter.StartLeScan (this);
		}

		void StopScan ()
		{
			adapter.StopLeScan (this);	
		}

		public bool RequestEnableBluetooth (Activity context)
		{
			if (adapter == null || !adapter.IsEnabled) {
//				Bluetooth is disabled
				Intent enableBluetoothIntent = new Intent (BluetoothAdapter.ActionRequestEnable);
				context.StartActivity (enableBluetoothIntent);
				context.Finish ();
				return false;
			}

			return true;
		}
	}
}

