﻿using System;
using Android.Bluetooth;

namespace emotiv_app.Droid
{
	public static class BluetoothExtensions
	{
		public static bool IsBB8 (this BluetoothDevice device)
		{
			if (device == null || device.Name == null) {
				return false;
			}

			return device.Name.StartsWith ("BB-");
		}
	}
}

