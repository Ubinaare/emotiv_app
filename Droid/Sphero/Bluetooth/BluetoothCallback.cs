﻿using System;
using Android.Bluetooth;

namespace emotiv_app.Droid
{
	public class BluetoothCallback : BluetoothGattCallback
	{
		public EventHandler<EventArgs> OnCharacteristicWritten;
		public EventHandler<EventArgs> OnServiceDiscover;

		public BluetoothCallback () 
		{
			
		}

		public override void OnConnectionStateChange (BluetoothGatt gatt, GattStatus status, ProfileState newState)
		{
			Console.WriteLine ("OnConnectionStateChange: ", newState.ToString());

			base.OnConnectionStateChange (gatt, status, newState);

			gatt.DiscoverServices ();
		}

		public override void OnServicesDiscovered (BluetoothGatt gatt, GattStatus status)
		{
			foreach (BluetoothGattService service in gatt.Services) {
				Console.WriteLine ("Service: " + service.Uuid);
				Console.WriteLine ("Characteristics: ");

				foreach (BluetoothGattCharacteristic characteristic in service.Characteristics) {
					Console.WriteLine (characteristic.Uuid);	
				}
				Console.WriteLine ("-------------------------------------------");
			}

			if (OnServiceDiscover != null) {
				OnServiceDiscover (new object (), new EventArgs ());
			}

		}

		public override void OnCharacteristicRead (BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, GattStatus status)
		{
			Console.WriteLine ("OnCharacteristicRead: " + characteristic);

			byte[] response = characteristic.GetValue ();

			foreach (byte _byte in response) {
				Console.WriteLine (_byte.ToString ());
			}
			base.OnCharacteristicRead (gatt, characteristic, status);
		}

		public override void OnDescriptorRead (BluetoothGatt gatt, BluetoothGattDescriptor descriptor, GattStatus status)
		{
			Console.WriteLine ("OnDescriptorRead: " + descriptor);
			base.OnDescriptorRead (gatt, descriptor, status);
		}

		public override void OnCharacteristicWrite (BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, GattStatus status)
		{
			if (OnCharacteristicWritten != null) {
				OnCharacteristicWritten (new object (), new EventArgs ());
			}

			byte[] response = characteristic.GetValue ();

			foreach (byte _byte in response) {
				Console.WriteLine (_byte.ToString ());
			}

			base.OnCharacteristicWrite (gatt, characteristic, status);
		}


	}
}

