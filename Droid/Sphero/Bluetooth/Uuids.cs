﻿using System;
using Java.Util;

namespace emotiv_app.Droid
{
	public class Uuids
	{
		public readonly static UUID AntiDOSCharacteristic = UUID.FromString("22bb746f-2bbd-7554-2d6f-726568705327");
		public readonly static UUID TXPowerCharacteristic = UUID.FromString("22bb746f-2bb2-7554-2d6f-726568705327");
		public readonly static UUID WakeCPUCharacteristic = UUID.FromString("22bb746f-2bbf-7554-2d6f-726568705327");

		public readonly static UUID WakeupService = UUID.FromString("22bb746f-2bb0-7554-2d6f-726568705327");


		public readonly static UUID RollService = UUID.FromString("22bb746f-2ba0-7554-2d6f-726568705327");

		public readonly static UUID RollCharacteristic = UUID.FromString("22bb746f-2ba1-7554-2d6f-726568705327");
	}
}

