﻿using System;
using Java.Util;

namespace emotiv_app.Droid
{
	public class WriteInformation
	{
		public UUID Characteristic { get; set; }

		public UUID Service { get; set; }

		public byte[] Data { get; set; }

		public bool IsSent { get; set; }

		public WriteInformation ()
		{
			
		}
	}
}

