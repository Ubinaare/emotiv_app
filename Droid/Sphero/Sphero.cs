﻿using System;
using Android.App;
using System.Collections.Generic;

namespace emotiv_app.Droid
{
	public class Sphero
	{
		public string Address { get; internal set; }

		public BluetoothConnection Connection { get; internal set; }

		public Activity Activity { get; internal set; }

		List<WriteInformation> WakeUpSequence;

		public Sphero (Activity context, string address)
		{
			Activity = context;

			Address = address;

			Connection = new BluetoothConnection (this);

			byte[] antidos = { (byte)'0', (byte)'1', (byte)'1', (byte)'i', (byte)'3' };
			byte[] txpower = { 0, (byte)'0', (byte)'7' };
			byte[] wakecpu = { 1 };

			WakeUpSequence = new List<WriteInformation> {
				new WriteInformation {
					Data = antidos,
					Characteristic = Uuids.AntiDOSCharacteristic,
					Service = Uuids.WakeupService
				},
				new WriteInformation {
					Data = txpower,
					Characteristic = Uuids.TXPowerCharacteristic,
					Service = Uuids.WakeupService
				},
				new WriteInformation {
					Data = wakecpu,
					Characteristic = Uuids.WakeCPUCharacteristic,
					Service = Uuids.WakeupService
				},
			};
		}

		public void WakeUp ()
		{
			Connection.Queue (WakeUpSequence);
		}

//		char[] CMD_SET_LED = { '0x02', '0x20' };

		public void SetLEDColor (Android.Graphics.Color Color)
		{
			
		}

		public void Roll ()
		{
			WriteInformation roll = new WriteInformation {
				Service = Uuids.RollService,
				Characteristic = Uuids.RollCharacteristic,
				Data = GetRollData (new byte[] { 0, 80, 0, 1, 0x80, 0, 0, 0,   0 })
			};

			Connection.Queue (new List<WriteInformation> { roll });
		}

		int sequence = 0;

		byte[] GetRollData (byte[] data)
		{
			byte init = 0xff;
			byte sop2 = 0xfc;

			byte did = 0x02;
			byte cid = 0x20;

			byte seqByte = (byte)sequence;
			sequence++;

			byte dataLength = 10;

			byte chk = 255;

			byte[] array = new byte[] {
				init,
				sop2,
				did,
				cid,
				seqByte,
				dataLength,
			};
			Console.WriteLine (array.Length);
			int pos = array.Length;

			foreach (byte item in data) {
				array [pos] = item;
				pos++;
			}

			Console.WriteLine (array.Length);

			return array;

		}


	}
}

