﻿using System;
using Android.App;
using Android.Widget;

namespace emotiv_app.Droid
{
	[Activity (Label = "Emotiv", MainLauncher = true, Icon = "@mipmap/icon")]
	public class SpheroMainActivity : Activity
	{
		const string ADDRESS = "D0:B7:8B:34:A3:FD";

		public MainView contentView;

		Sphero sphero;

		protected override void OnCreate (Android.OS.Bundle savedInstanceState)
		{
			Device.Measure (this);

			base.OnCreate (savedInstanceState);

			contentView = new MainView (this);
			SetContentView (contentView);
			contentView.LayoutSubviews ();

			sphero = new Sphero (this, ADDRESS);
		}

		protected override void OnResume ()
		{
			base.OnResume ();

			sphero.Connection.OnConnected += UpdateConnectedLabel;

			sphero.Connection.Callback.OnServiceDiscover += UpdateServicesLabel;

			contentView.WakeupButton.Click += WakeUp;
			contentView.ColorButton.Click += SendNewValue;

			sphero.Connection.RequestEnableBluetooth (this);
			sphero.Connection.Scan ();
		}

		protected override void OnPause ()
		{
			base.OnPause ();

			sphero.Connection.OnConnected -= UpdateConnectedLabel;

			sphero.Connection.Callback.OnServiceDiscover -= UpdateServicesLabel;

			contentView.WakeupButton.Click -= WakeUp;
			contentView.ColorButton.Click -= SendNewValue;
		}

		void UpdateConnectedLabel (object sender, EventArgs e)
		{
			contentView.UpdateConnectedText ();	
		}

		void UpdateServicesLabel (object sender, EventArgs e)
		{
			RunOnUiThread (delegate {
				contentView.UpdateServicesText ();
			});
		}

		void WakeUp (object sender, EventArgs e)
		{
			sphero.WakeUp ();
		}

		void SendNewValue (object sender, EventArgs e)
		{
//			sphero.SetLEDColor (Android.Graphics.Color.Blue);
			sphero.Roll ();
		}

	}
}

