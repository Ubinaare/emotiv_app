﻿using Android.App;
using Android.Widget;
using Android.OS;
using Java.Util;
using Android.Bluetooth;
using System;
using Android.Content;
using System.Globalization;
using System.Collections.Generic;

namespace emotiv_app.Droid
{
//	[Activity (Label = "Emotiv", MainLauncher = true, Icon = "@mipmap/icon")]
	public class MainActivity : Activity, BluetoothAdapter.ILeScanCallback
	{
//		https://gist.github.com/ali1234/5e5758d9c591090291d6
//		https://github.com/mmwise/sphero_ros/blob/groovy-devel/sphero_driver/src/sphero_driver/sphero_driver.py

		const string ROBOT_SERVICE = "22bb746f-2ba0-7554-2d6f-726568705327";
		const string ROBOT_CHAR_COMMAND = "22bb746f-2ba1-7554-2d6f-726568705327";
		const string ROBOT_CHAR_NOTIFY = "22bb746f-2ba6-7554-2d6f-726568705327";

		readonly static UUID ANTIDOSCHARACTERISTIC = UUID.FromString("22bb746f-2bbd-7554-2d6f-726568705327");
		readonly static UUID TXPOWERCHARACTERISTIC = UUID.FromString("22bb746f-2bb2-7554-2d6f-726568705327");

		const string WAKEUPSERVICE = "22bb746f-2bb0-7554-2d6f-726568705327";
		readonly static UUID WAKEUPSERVICEUUID = UUID.FromString(WAKEUPSERVICE);
		const string WAKEUPCHAR = "22bb746f-2bbf-7554-2d6f-726568705327";
		readonly static UUID WAKEUPCHARUUID = UUID.FromString(WAKEUPCHAR);

		const string BLE_RADIO_SERVICE = "22bb746f2bb075542d6f726568705327";
		const string BLE_RADIO_CHAR_RSSI = "22bb746f2bb675542d6f726568705327";

		const string DEVICE_INFO_SERVICE = "180a";
		const string DVC_INFO_CHAR_MANUFACTURER = "2a29";

//		readonly static  UUID SERVICE_UUID = UUID.FromString("00001861-B87F-490C-92CB-11BA5EA5167C");
//		readonly static  UUID CHARACTERISTIC_UUID = UUID.FromString("00001525-B87F-490C-92CB-11BA5EA5167C");

		readonly static  UUID SERVICE_UUID = UUID.FromString(ROBOT_SERVICE);
		readonly static  UUID CHARACTERISTIC_UUID = UUID.FromString(ROBOT_CHAR_COMMAND);

		// 81072f40-9f3d-11e3-a9dc-0002a5d5c51b
		// 
		private BluetoothGatt mConnectedGatt;
		private BluetoothAdapter mBluetoothAdapter;
		private BluetoothDevice mDevice;

		MainView contentView;

		protected override void OnCreate (Bundle savedInstanceState)
		{
			Device.Measure (this);

			base.OnCreate (savedInstanceState);

			contentView = new MainView (this);
			contentView.LayoutSubviews ();
			SetContentView (contentView);

			BluetoothManager manager = (BluetoothManager)GetSystemService (BluetoothService);
			mBluetoothAdapter = manager.Adapter;

			Client.Instance.Subscribe (OnMessageReceived);
		}

		void OnMessageReceived (SendData data)
		{
			contentView.BackgroundColor = Android.Graphics.Color.Red;
		}

		protected override void OnResume ()
		{
			base.OnResume ();

			contentView.WakeupButton.Click += SendData;

			/*
        	 * We need to enforce that Bluetooth is first enabled, and take the
         	* user to settings to enable it if they have not done so.
         	*/

			if (mBluetoothAdapter == null || !mBluetoothAdapter.IsEnabled) {
				//Bluetooth is disabled
				Intent enableBluetoothIntent = new Intent (BluetoothAdapter.ActionRequestEnable);
				StartActivity (enableBluetoothIntent);
				Finish ();
				return;
			}

			/*
         	* Check for Bluetooth LE Support.  In production, our manifest entry will keep this
         	* from installing on these devices, but this will allow test devices or other
         	* sideloads to report whether or not the feature exists.
         	*/

			if (!PackageManager.HasSystemFeature("android.hardware.bluetooth_le")) {
				Toast.MakeText (this, "No Bluetooth Low Energy support.", ToastLength.Short).Show ();
				Finish ();
				return;
			}

			StartScan ();
		}

		protected override void OnPause ()
		{
			base.OnPause ();

			contentView.WakeupButton.Click -= SendData;
		}

		void SendData (object sender, EventArgs e)
		{
			SendData ();
		}

		int counter = -1;

		void SendData ()
		{
			counter++;

			if (counter == 0) {
				Console.WriteLine ("Sending ANTIDOSCHARACTERISTIC");
//				SendData (mDevice, (STARTUPSEQUENCE[0] as string).ToBytes (), ANTIDOSCHARACTERISTIC);
				SendData (mDevice, antidos, ANTIDOSCHARACTERISTIC);
			} else if (counter == 1) {
				Console.WriteLine ("Sending TXPOWERCHARACTERISTIC");
//				SendData (mDevice, new byte[] { (byte)(char)STARTUPSEQUENCE[1] }, TXPOWERCHARACTERISTIC);
				SendData (mDevice, txpower, TXPOWERCHARACTERISTIC);
			} else if (counter == 2) {
				Console.WriteLine ("Sending WAKEUPCHARUUID");
//				SendData (mDevice, new byte[] { (byte)(char)STARTUPSEQUENCE[2] }, WAKEUPCHARUUID);
				SendData (mDevice, wakecpu, WAKEUPCHARUUID);
			} else {
				Console.WriteLine ("Done");
				if (counter > 2) {
					counter = -1;
				}
			}

		}

		public void OnLeScan (BluetoothDevice device, int rssi, byte[] scanRecord)
		{
			Console.WriteLine ("device_name: " + device.Name + "; Address: " + device.Address);

			if (device.IsBB8 ()) {
				mDevice = device;
				Console.WriteLine ("CONNECTED");
				//Stops scan once it finds one device
				mBluetoothAdapter.StopLeScan (this);
				ConnectGatt ();
			} else {
				Console.WriteLine ("Device has no name");
			}
		}


		private void StartScan () 
		{
			mBluetoothAdapter.StartLeScan (this);
		}

		private void ConnectGatt()
		{
			RunOnUiThread (delegate {
				mConnectedGatt = mDevice.ConnectGatt (this, true, new BGattCallback (this));
			});
		}

//		object[] STARTUPSEQUENCE = {"011i3", '\x0007', '\x01'};

//		char[] STARTUPSEQUENCE = {'011', '\x0007', '\x01'};

		byte[] antidos = { (byte)'0', (byte)'1', (byte)'1', (byte)'i', (byte)'3' };
		byte[] txpower = { 0, (byte)'0', (byte)'7' };
		byte[] wakecpu = { 1 };

//		List<byte[]> STARTUPSEQUENCE = new List<byte[]> {
//			antidos, txpower, wakecpu
//		};

//		byt[][] STARTUPSEQUENCE = {
//			{ '0', '1', '1', 'i', '3' },
//			{ 0, '0', '7' },
//			{ 1 }
//		};
			
		public void SendData(BluetoothDevice device, byte[] data, UUID characteristicUUID)
		{
//			BluetoothGattService service = mConnectedGatt.GetService (SERVICE_UUID);
			BluetoothGattService service = mConnectedGatt.GetService (WAKEUPSERVICEUUID);

			if (service == null) {
				Console.WriteLine ("Service is empty");
				return;
			}

//			BluetoothGattCharacteristic characteristic = service.GetCharacteristic (CHARACTERISTIC_UUID);
			BluetoothGattCharacteristic characteristic = service.GetCharacteristic (characteristicUUID);

			if (characteristic == null) {
				Console.WriteLine ("Characteristic empty");
				return;
			}

//			byte[] array = {(byte)number};
//			byte[] array = {(byte)'\x01'};

//			byte[] array = (STARTUPSEQUENCE[0] as string).ToBytes ();
//			byte[] array = {(byte)STARTUPSEQUENCE[0]};

			characteristic.SetValue (data);

			mConnectedGatt.WriteCharacteristic (characteristic);
		}

		public void Continue ()
		{
			SendData ();
		}
	}
}


